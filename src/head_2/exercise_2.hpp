/**
 * @file tasks::head_2::exercise_2.hpp
 *
 * @brief Declaration of classes @ref tasks::head_2::exercise_2
 */

#ifndef TASKS_SRC_HEAD_1_EXERCISE_1_HPP
#define TASKS_SRC_HEAD_1_EXERCISE_1_HPP

//include standard libraries
#include <iostream>
#include <fstream>
#include <vector>

namespace tasks {
        namespace head_2 {
                namespace exercise_2 {
                        /**
                         * @brief information of customer
                         * @param account namber
                         * @param starting balance
                         * @param amount of expenses
                         * @param amount received
                         * @param last balance
                         * @param credit limit
                         */
                        struct account_user;
                        /**
                         * @brief vector in class account_user
                         * @param vector account_user
                         */
                        class bank;
                }
        }
}

struct tasks::head_2::exercise_2::
        account_user
{
private:
        //parameters
        int account_number;
        int starting_balance;
        int amount_of_expenses;
        int amount_received;
        int last_balance;
        int credit_limit;
public:
        //constructor
        account_user()
        {
                account_number = 0;
                starting_balance = 0;
                amount_of_expenses = 0;
                amount_received = 0;
                last_balance = 0;
                credit_limit = 0;
        }
        //destructor
        ~account_user(){}
        //get set
        int get_account_number()
        {
                return account_number;
        }
	void set_account_number(int a)
	{
		this -> account_number = a;
	}
        int get_starting_balance()
        {
                return starting_balance;
        }
	void set_starting_balance(int a)
	{
		this -> starting_balance = a;
	}
        int get_amount_of_expenses()
        {
                return amount_of_expenses;
        }
	void set_amount_of_expenses(int a)
	{
		this -> amount_of_expenses = a;
	}
        int get_amount_received()
        {
                return amount_received;
        }
	void set_amount_received(int a)
	{
		this -> amount_received = a;
	}
        int get_last_balance()
        {
                return last_balance;
        }
	void set_last_balance(int a)
	{
		this -> last_balance = a;
	}
        int get_credit_limit()
        {
                return credit_limit;
        }
	void set_credit_limit(int a)
	{
		this -> credit_limit = a;
	}

        int balance_end()
        {
                return get_starting_balance() - get_amount_of_expenses() +
                    get_amount_received();
        }
        // operator "in" in class account_user
        friend bool operator >> (std::istream&, account_user&);

	void true_false(){
        	if (last_balance > credit_limit) {
	                std::cout<< "credit limit exceeded" << std::endl;
        	} else {
                	std::cout<< "NO" << std::endl;
        	}
	}

};

class tasks::head_2::exercise_2::
        bank
{

private:
        std::vector<account_user> vec_card;

public:
        //operator "in" in class bank
        friend void operator >> (std::istream&, bank&);
};

#endif
