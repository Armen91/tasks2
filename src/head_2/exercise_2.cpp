#include "exercise_2.hpp"
#include <iostream>
#include <fstream>


bool tasks::head_2::exercise_2::
            operator >> (std::istream& in, account_user& a)
{
        std::cout << "Input: number account(-1 if input is completed):     ";
        int in_account_number;
        in >> in_account_number;
        if (-1 == in_account_number || 0 == in_account_number){
                return false;
        }
        a.account_number = in_account_number;
        std::cout << "Input: initial balance:     ";
        int in_bal;
        in >> in_bal;
        a.starting_balance = in_bal;
        std::cout << "Input: amount of expenses:     ";
        int in_amount_of_expenses;
        in >> in_amount_of_expenses;
        a.amount_of_expenses = in_amount_of_expenses;
        std::cout << "Input: amount received:     ";
        int in_amount_received;
        in >> in_amount_received;
        a.amount_received = in_amount_received;
        a.last_balance = a.balance_end();
        std::cout << "Input: limit credit:     ";
        int in_pred_cred;
        in >> in_pred_cred;
        a.credit_limit = in_pred_cred;
        a.last_balance = a.balance_end();
        return true;
}
void tasks::head_2::exercise_2::operator >> (std::istream& in, bank& a)
{
        std::cout << std::endl;
        bool b = true;
        tasks::head_2::exercise_2::account_user c_u;
        while (b) {
                b = std::cin >> c_u;
                if (b) {
                        a.vec_card.push_back(c_u);
                        std::cout << "\naccount_number:      " << c_u.get_account_number() << std::endl;
                        std::cout << "limit credit:     " << c_u.get_credit_limit() << std::endl;
                        std::cout << "last balance:      " << c_u.get_last_balance() << std::endl;
                        c_u.true_false();
                }
        }
}

