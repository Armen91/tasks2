#include "exercise_1.hpp"
#include <iostream>

int tasks::head_2::exercise_1::
	largest(int *arr, int counter)
{
	int temp = 0;
	for(int i = 0; i < counter; ++i) {
		arr[i] = rand() % 1000;
		if(temp < arr[i]) {
			temp = arr[i];
		}
	}
	return temp;
}
