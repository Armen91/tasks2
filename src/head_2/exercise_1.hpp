/*
 * @file tasks/src/head_2/exercise_1.hpp
 *
 * @brief Declaration of function @ref tasks::head_2::exercise_1::largest
 */

#ifndef TASKS_HEAD_2_EXERCISE_1_HPP
#define TASKS_HEAD_2_EXERCISE_1_HPP

//include of standard libraries
#include <iostream>
#include <cstdlib>

namespace tasks {
        namespace head_2 {
                namespace exercise_1 {
                        /*
                         * @brief a find greatest value
                         * @param *arr array in int
                         * @param counter of array
                         * @return greatest value
                         */
                        int largest(int *arr, int counter);
                }
        }
}

#endif
