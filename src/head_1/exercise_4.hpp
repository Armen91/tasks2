/**
 * @file tasks/src/head_1/exercise_4.hpp
 *
 * @brief Declaration of functions @ref tasks::exercise_4::
 */

#ifndef TASKS_SRC_HEAD_1_EXERCISE_1_HPP
#define TASKS_SRC_HEAD_1_EXERCISE_1_HPP

//include from standard libraries
#include <iostream>

namespace tasks {
        namespace head_1 {
                namespace exercise_4 {
                        /*
                         * @brief will draw rectangle
                         */
                        void rectangle();
                        /*
                         * @brief will draw oval
                         */
                        void oval();
                        /*
                         * @brief will draw arrow
                         */
                        void arrow();
                        /*
                         * @brief will draw rhombus
                         */
                        void rhumbus();
                }
        }
}

#endif
