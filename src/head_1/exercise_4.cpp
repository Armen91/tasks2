#include "exercise_4.hpp"

void star_(int a, int b)
{
	for(int i = 0; i < a; ++i) {
		if(b - 1 == i || i == a - b){
			std::cout << "*";
		} else {
			std::cout << " ";
		}
	}
	std::cout << std::endl;
}
	
void star(int a, int b)
{
	for(int i = 0; i < a; ++i) {
		if(b <= i && i < a - b){
			std::cout << "*";
		} else {
			std::cout << " ";
		}
	}
	std::cout << std::endl;
}
	
void tasks::head_1::exercise_4::
	rectangle()
{
	star(9, 0);
	for(int i = 0; i < 7; ++i) {
		star_(9, 1);
	}
	star(9, 0);
}

void tasks::head_1::exercise_4::
	oval()
{
	int b = 3;
	int a = 9;
	star(a, b);
	star_(a, b - 1);
	for(int i = 0; i < 5; ++i){
		star_(a, 1);
	}
	star_(a, b - 1);
	star(a, b);
}

void tasks::head_1::exercise_4::
	arrow()
{
	int a = 9;
	for(int i = 0; i < 9; ++i){
		if(i < 3){
			star(a, a / 2 - i);
		} else {
			star(a, a / 2);
		}
	}
}

void tasks::head_1::exercise_4::
	rhumbus()
{
	std::cout<<std::endl;
	int a = 9;
	for(int i = a / 2; i > 0; --i){
		star_(a, i + 1);
	}
	for(int i = 0; i <= a / 2; ++i){
		star_(a, i + 1);
	}
}
