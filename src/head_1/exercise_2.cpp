#include "exercise_2.hpp"

void tasks::head_1::exercise_2::
	print_1()
{
	for(int i = 1; i < 5; ++i) {
		std::cout << i << " ";
	}
}
void tasks::head_1::exercise_2::
	print_2()
{
	int i = 4;
	std::cout << "\n" << i-- << " " << i-- << " " << i-- << " " << i-- << "\n";
}
void tasks::head_1::exercise_2::
	print_3()
{
	std::cout << "1 ";
	std::cout << "2 ";
	std::cout << "3 ";
	std::cout << "4 \n";
}
