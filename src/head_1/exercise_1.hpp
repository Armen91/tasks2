/**
 * @file tasks/src/head_1/exercise_1.hpp
 *
 * @brief Declaration of function @ref tasks::head_1::exercise_1::sum_and_diff
 */

#ifndef TASKS_SRC_HEAD_1_EXERCISE_1_HPP
#define TASKS_SRC_HEAD_1_EXERCISE_1_HPP

//include from standard libraries
#include <iostream>


namespace tasks {
        namespace head_1 {
                namespace exercise_1 {
                        /**
                         * @brief calculate of sum and diff
                         * @param num1 first number
                         * @param num2 second number
                         * @return sum and diff pair
                         */
                        std::pair<int, int>
                        sum_and_diff(const int num1, const int num2);
                }
        }
}

#endif
