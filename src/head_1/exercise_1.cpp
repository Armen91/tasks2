#include "exercise_1.hpp"


#include <iostream>

std::pair<int, int> tasks::head_1::exercise_1::
	sum_and_diff(const int num_1, const int num_2)
{
	std::pair<int, int> res;
	res.first = num_1 + num_2;
	res.second = num_1 - num_2;
	return res;
}
