/**
 * @file tasks/src/head_1/exercise_3.hpp
 *
 * @brief Declaration of function @ref tasks::head_1::exercise_3::more
 *
 */

#ifndef TASKS_SRC_HEAD_1_EXERCISE_3_HPP
#define TASKS_SRC_HEAD_1_EXERCISE_3_HPP

//include from standard libraries
#include <iostream>

namespace tasks {
        namespace head_1 {
                namespace exercise_3 {
                        /**
                         * @brief a find greatest value
                         * @return greatest value
                         */
                        int more();
                }
        }
}

#endif
