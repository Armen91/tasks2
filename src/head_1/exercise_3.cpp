#include "exercise_3.hpp"
#include <iostream>

int tasks::head_1::exercise_3::
	more()
{
	int num_1 = 365;
	int num_2 = 452;
	if(num_1 == num_2) {
		std::cout << "flat" <<std::endl;
		return num_1;
	} else if(num_1 > num_2) {
		return num_1;
	} else {
		return num_2;
	}
}
