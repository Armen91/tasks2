/**
 * @file tasks/src/head_1/exercise_2.hpp
 *
 * @brief Declaration of functions in namespace @ref tasks::head_1::exercise_2::
 */

#ifndef TASKS_SRC_HEAD_1_EXERCISE_2_HPP
#define TASKS_SRC_HEAD_1_EXERCISE_2_HPP

//include from standard libraries
#include <iostream>
namespace tasks {
        namespace head_1 {
                namespace exercise_2 {
                        /**
                         * @brief outputs
                         */
                        void print_1();
                        /**
                         * @brief outputs
                         */
                        void print_2();
                        /**
                         * @brief outputs
                         */
                        void print_3();
                }
        }
}

#endif
