MAKE_DIR = $(PWD)


SRC_DIR  := $(MAKE_DIR)/src
TESTS_DIR := $(MAKE_DIR)/tests

LIBS := -ltasks

export MAKE_DIR
export LIBS

INC_SRCH_PATH :=
INC_SRCH_PATH += -I$(SRC_DIR)
INC_SRCH_PATH += -I$(TESTS_DIR)

all:
	$(MAKE) -C src
	$(MAKE) -C tests

clean:
	$(MAKE) clean -C src
	$(MAKE) clean -C tests
