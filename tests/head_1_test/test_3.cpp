// Headers from this project
#include "test_3.hpp"

// Headers from other projects
#include <head_1/exercise_3.hpp>

// Headers from standard libraries
#include <iostream>
#include <cassert>

void head_1_test_3()
{
	assert(452 == tasks::head_1::exercise_3::more());
        std::cout << "Head_1 Exercise_3: more " << "PASSED" << std::endl;
}
