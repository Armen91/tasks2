/**
 * @file tasks/tests/head_1_test/test_1.hpp
 *
 * @brief Declaration of function @ref tasks/tests/head_1_test/head_1_test_1
 */

#ifndef TASKS_TESTS_HEAD_1_TEST_1_HPP
#define TASKS_TESTS_HEAD_1_TEST_1_HPP

/// @brief This function tests tasks::head_1::exercise_1 function
void head_1_test_1();

#endif
