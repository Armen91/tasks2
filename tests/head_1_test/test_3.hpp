/**
 * @file tasks/tests/head_1_test/test_3.hpp
 *
 * @brief Declaration of function @ref tasks/tests/head_1_test/head_1_test_3
 */

#ifndef TASKS_TESTS_HEAD_1_TEST_3_HPP
#define TASKS_TESTS_HEAD_1_TEST_3_HPP

/// @brief This function tests tasks::head_1::exercise_3 function
void head_1_test_3();

#endif
