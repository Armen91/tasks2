// Headers from this project
#include "test_1.hpp"

// Headers from other projects
#include <head_1/exercise_1.hpp>

// Headers from standard libraries
#include <iostream>
#include <cassert>

void head_1_test_1()
{
	std::pair<int, int> result = tasks::head_1::exercise_1::sum_and_diff(5, 1);
	assert(result.first == 6);
	assert(result.second == 4);
	std::cout << "Head_1 Exercise_1: sum_and_diff " << "PASSED" << std::endl;
}
