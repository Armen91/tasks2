// Headers from this project
#include "test_4.hpp"

// Headers from other projects
#include <head_1/exercise_4.hpp>

// Headers from standard libraries
#include <iostream>
#include <cassert>


void head_1_test_4()
{
//	tasks::head_1::exercise_4::rectangle();
//	tasks::head_1::exercise_4::oval();
//	tasks::head_1::exercise_4::arrow();
//	tasks::head_1::exercise_4::rhumbus();
        std::cout << "Head_1 Exercise_4: cout rectangle/oval/arrow/rhumbus "
            << "PASSED" << std::endl;
}
