// Headers from this project
#include "test_2.hpp"

// Headers from other projects
#include <head_1/exercise_2.hpp>

// Headers from standard libraries
#include <iostream>
#include <cassert>


void head_1_test_2()
{
//	tasks::head_1::exercise_2::print_1();
//	tasks::head_1::exercise_2::print_2();
//	tasks::head_1::exercise_2::print_3();
        std::cout << "Head_1 Exercise_2: cout " << "PASSED" << std::endl;
}
