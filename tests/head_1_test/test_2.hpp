/**
 * @file tasks/tests/head_1_test/test_2.hpp
 *
 * @brief Declaration of function @ref tasks/tests/head_1_test/head_1_test_2
 */


#ifndef TASKS_TESTS_HEAD_1_TEST_2_HPP
#define TASKS_TESTS_HEAD_1_TEST_2_HPP

/// @brief This function tests tasks::head_1::exercise_2 function
void head_1_test_2();

#endif
