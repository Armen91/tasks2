#include <head_1_test/test_1.hpp>
#include <head_1_test/test_2.hpp>
#include <head_1_test/test_3.hpp>
#include <head_1_test/test_4.hpp>
#include <head_2_test/test_1.hpp>
#include <head_2_test/test_2.hpp>


int main(int, char**)
{
	head_1_test_1();
	head_1_test_2();
	head_1_test_3();
	head_1_test_4();
	head_2_test_1();
	head_2_test_2();
	return 0;
}
