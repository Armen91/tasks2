// Headers from this project
#include "test_1.hpp"

// Headers from other projects
#include <head_2/exercise_1.hpp>

// Headers from standard libraries
#include <iostream>
#include <cassert>

void head_2_test_1()
{
        int counter = 10;
        int arr[10];
        counter = tasks::head_2::exercise_1::largest(arr, counter);
        assert(915 == counter);
        std::cout << "Head_2 Exercise_1: largest " << "PASSED" << std::endl;
}
