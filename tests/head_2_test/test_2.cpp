// Headers from this project
#include "test_2.hpp"

// Headers from other projects
#include <head_2/exercise_2.hpp>

// Headers from standard libraries
#include <iostream>


void head_2_test_2()
{
        tasks::head_2::exercise_2::account_user b;
	b.set_account_number(256365);
	b.set_starting_balance(15000);
	b.set_amount_of_expenses(3600);
	b.set_amount_received(3000);
	b.set_last_balance(6000);
        std::cout << "Head_2 Exercise_2: struct accound_user "
            << "PASSED" << std::endl;

}
