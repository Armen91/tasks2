/**
 * @file tasks/tests/head_2_test/test_1.hpp
 *
 * @brief Declaration of function @ref tasks/tests/head_2_test/head_2_test_1
 */

#ifndef TASKS_TESTS_HEAD_2_TEST_1_HPP
#define TASKS_TESTS_HEAD_2_TEST_1_HPP

/// @brief This function tests tasks::head_2::exercise_1 function
void head_2_test_1();

#endif

