/**
 * @file tests/head_2_test/test_2.hpp
 *
 * @brief Declaration of function @ref tasks/tests/head_2_test/head_2_test_2
 */

#ifndef TASKS_TESTS_HEAD_2_TEST_2_HPP
#define TASKS_TESTS_HEAD_2_TEST_2_HPP

/// @brief This function tests tasks::head_2::exercise_2 function
void head_2_test_2();

#endif

